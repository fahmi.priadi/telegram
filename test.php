<?php

//add PHPMailer for sending email 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './src/Exception.php';
require './src/PHPMailer.php';
require './src/SMTP.php';

  $botToken="";

  $website="https://api.telegram.org/bot".$botToken;

  $api_reg="https://nd04.plnbabel.co.id/telegram/api.php";


  $servername = "10.30.1.25";
  $username = "telegram";
  $password = "P@ssw0rd";
  $database = "telegram_babel";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $database);
  // Check connection
  
  // read incoming info and grab the chatID

$content = file_get_contents("php://input");
$update = json_decode($content, true);
$chatID = $update["message"]["chat"]["id"];
$input_user = $update["message"]["text"]; 

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    $params=[
        //'chat_id'=>$chatId,
        'chat_id'=>$chatID, 
        'text'=> 'Database Error',
    ];
} 

//fork when text send by user is an email or other text
if (preg_match("/([a-z0-9]*)\@pln.co.id/i", $input_user)) {
    
    //Check whether the email supplied is already registered
    $email = $input_user ;
    $sql = "SELECT * FROM mapping_user WHERE email_korporat=".$email ;
    
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        //$row = $result->fetch_assoc() 
        
        $params=[
            //'chat_id'=>$chatId,
            'chat_id'=>$chatID, 
            'text'=> 'User telah terdaftar, terima kasih',
        ];
    } else {
        //if email couldnt be found in database, then send link to authorizing the process of registering email
        
        //but first insert the email into database with random number as and ID
        //confirmation link will be used to change the random number inserted with actual user's telegram ID
        $email = $input_user;
        $arbitrary_id = rand(1000, 9999);

        $sql = "INSERT INTO mapping_user (email_korporat, user_id) VALUES ('". $email ."', '". $arbitrary_id ."')";
        
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
            
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        
        }

        $url_api = $api_reg .  '/' . $chatID . '/' . $arbitrary_id ;

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'hub.pln.co.id';  // Specify main and backup SMTP servers
            $mail->Port = 25;                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('wbabel@pln.co.id', 'Admin Telegram');
            $mail->addAddress($email);               // Name is optional
            
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Registrasi Telegram PLN Babel';
            $mail->Body    = 'Klik link berikut untuk registrasi: <a href="'.$url_api.'">Link Registrasi</a>';
            $mail->AltBody = 'Kopi link berikut ke browser anda: '.$url_api ;
        
            $mail->send();

            $params=[
                //'chat_id'=>$chatId,
                'chat_id'=>$chatID, 
                'text'=> 'Konfirmasi telah kami kirimkan ke email anda',
            ];
        } catch (Exception $e) {
            $params=[
                //'chat_id'=>$chatId,
                'chat_id'=>$chatID, 
                'text'=> 'Email gagal kirim. Periksa kembali email anda',
            ];
        } 
    }
}
else {
    //Greetings for registered user
    $sql = "SELECT * FROM mapping_user WHERE user_id=".$chatID ;
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        
        $row = $result->fetch_assoc() ;
        $params=[
            //'chat_id'=>$chatId,
            'chat_id'=>$chatID, 
            'text'=> 'Selamat datang, email anda: ' . $row["email_korporat"],
        ];
        
    } else {
        

        $params=[
            //'chat_id'=>$chatId,
            'chat_id'=>$chatID, 
            'text'=> 'Selamat datang, email anda belum terdaftar. Mohon masukkan email korporat anda',
        ];
            
    }
}
  $ch = curl_init($website . '/sendMessage');
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  $result = curl_exec($ch);
  curl_close($ch);
  echo $result;

  mysqli_close($conn);
?> 